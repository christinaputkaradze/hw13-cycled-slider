let stopBtn;
let continueBtn;

let startBtn = document.createElement('button');
startBtn.innerText = 'Старт';
document.body.prepend(startBtn);

startBtn.addEventListener('click', () => {
    if (!stopBtn) {
        stopBtn = document.createElement('button');
        stopBtn.innerText = 'Припинити';
        document.body.append(stopBtn);
    }

    startBtn.disabled = true;

    let currentImg = 0;
    let timerId = setInterval(showImg, 3000);

    startBtn.disabled = false;

    stopBtn.addEventListener('click', () => {
        clearInterval(timerId);

        if (!continueBtn) {
            continueBtn = document.createElement('button');
            continueBtn.innerText = 'Відновити показ';
            document.body.append(continueBtn);
        }

        continueBtn.addEventListener('click', () => {
            timerId = setInterval(() => showImg(), 3000);
            continueBtn.remove();
            continueBtn = null;
        });

    })

    function showImg() {
        let imgCollection = document.querySelectorAll('.image-to-show');

        let oldVisible = document.querySelector('.visible');
        oldVisible.classList.remove('visible');

        currentImg++;
        
        if (currentImg === imgCollection.length) {
            currentImg = 0;
        }

        imgCollection[currentImg].classList.add('visible');

    }
})


